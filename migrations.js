const crypto = require('crypto');

module.exports = async keystone => {
  // This migration is needed to fill readAccess and writeAccess attributes of 
  // trees from treeUsers table which will be deleted in future
  console.log('\nMigration started');
  const response = await keystone.executeGraphQL({
    query: `query getList($where: TreeUserWhereInput) {      
      allTreeUsers(where: $where) {
          id
          user {id}
          tree {id}
        }          
      }`,
  });
  const trees = {};
  if(response.data.allTreeUsers !== undefined && response.data.allTreeUsers) {
    if (response.data.allTreeUsers.length > 0) {
      for (const treeUser of response.data.allTreeUsers) {
        if (!treeUser.tree) {
          continue;
        }
        const treeId = treeUser.tree.id;
        if (typeof trees[treeId] == 'undefined') {
          trees[treeId] = { readAccess: [], writeAccess: [] };
        }
        trees[treeId].readAccess.push(treeUser.user.id);
        trees[treeId].writeAccess.push(treeUser.user.id);
      }
      for (const treeId of Object.keys(trees)) {
        const tree = trees[treeId];
        await keystone.executeGraphQL({
          "operationName": "update",
          query: `mutation update($id: ID!, $data: TreeUpdateInput) {
              updateTree(id: $id, data: $data) {
                    id
                    __typename
                    }
                  }
                  `,
          variables: {
            "id": treeId, "data": {
              "readAccess": {
                "disconnectAll": true,
                "connect":
                  tree.readAccess.map(user => {
                    return { "id": user }
                  })
              },
              "writeAccess": {
                "disconnectAll": true,
                "connect":
                  tree.writeAccess.map(user => {
                    return { "id": user }
                  })
              }
            }
          },
        });
      }
    }
  }
  console.log('Migration finished');
};
