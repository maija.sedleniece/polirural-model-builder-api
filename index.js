const { Keystone } = require('@keystonejs/keystone');
const { PasswordAuthStrategy } = require('@keystonejs/auth-password');
const { Text, Checkbox, Password } = require('@keystonejs/fields');
const { GraphQLApp } = require('@keystonejs/app-graphql');
const { AdminUIApp } = require('@keystonejs/app-admin-ui');
const initialiseData = require('./initial-data');
const executeMigrations = require('./migrations');

const { MongooseAdapter: Adapter } = require('@keystonejs/adapter-mongoose');
const DB_SERVER = process.env.DB_SERVER || 'localhost';
const NO_AUTH_MODE = process.env.NO_AUTH_MODE == 'true' || process.env.NO_AUTH_MODE == 1;
const PROJECT_NAME = 'Polirural Model Builder API';
const adapterConfig = { mongoUri: `mongodb://${DB_SERVER}/polirural-model-builder-api` };
const lists = require('./lists/list-dictionary.js');
const expressSession = require('express-session');
const MongoStore = require('connect-mongo')(expressSession);

console.log('NOAUTHMODE', NO_AUTH_MODE);
console.log(adapterConfig);
const keystone = new Keystone({
  name: PROJECT_NAME,
  adapter: new Adapter(adapterConfig),
  cookieSecret: process.env.COOKIE_SECRET || 'hlehdsas3fs98fdkfh',
  sessionStore: new MongoStore({ url: `mongodb://${DB_SERVER}/polirural-model-builder-api` }),
  onConnect,
  secure: false,
  cookie: {
    secure: false, // Default to true in production
    maxAge: 1000 * 60 * 60 * 24 * 30, // 30 days
    sameSite: false,
  },
});

function onConnect(keystone){
  if(process.env.INITIALIZE_DATA) initialiseData(keystone);
  if(process.env.MIGRATE) executeMigrations(keystone);
}

const access = require('./access.js');
keystone.createList('User', {
  fields: {
    name: { type: Text },
    email: {
      type: Text,
      isUnique: true,
    },
    isAdmin: {
      type: Checkbox,
      // Field-level access controls
      // Here, we set more restrictive field access so a non-admin cannot make themselves admin.
      access: {
        update: access.userIsAdmin,
      },
    },
    password: {
      type: Password,
    },
  },
  // List-level access controls
  access: NO_AUTH_MODE ? undefined : {
    read: access.userIsRegistered,
    update: access.userIsAdminOrSelf,
    create: true,
    delete: access.userIsAdminOrSelf,
    auth: true,
  },
});

const authStrategy = keystone.createAuthStrategy({
  type: PasswordAuthStrategy,
  list: 'User',
});

const TreeSchema = require('./lists/tree.js');
lists.treeList = keystone.createList('Tree', TreeSchema);


const NodeSchema = require('./lists/node.js');
lists.nodeList = keystone.createList('Node', NodeSchema);

const ConnectionSchema = require('./lists/connection.js');
lists.connectionList = keystone.createList('Connection', ConnectionSchema);

const TreeUserSchema = require('./lists/treeUser.js');
lists.treeUserList = keystone.createList('treeUser', TreeUserSchema);

const NodeTypeSchema = require('./lists/nodeType.js');
lists.nodeTypeList = keystone.createList('nodeType', NodeTypeSchema);

const NodeTypeSetSchema = require('./lists/nodeTypeSet.js');
lists.nodeTypeSetList = keystone.createList('nodeTypeSet', NodeTypeSetSchema);

const BookmarkedTextSchema = require('./lists/bookmarkedText.js');
lists.bookmarkedTextList = keystone.createList('bookmarkedText', BookmarkedTextSchema);

const BookmarkedDocumentSchema = require('./lists/bookmarkedDocument.js');
lists.bookmarkedDocumentList = keystone.createList('bookmarkedDocument', BookmarkedDocumentSchema);

const basePath = process.env.BASE_PATH || '' ;

module.exports = {
  keystone,
  apps: [
    new GraphQLApp({
      apiPath: basePath  +  '/admin/api',
      graphiqlPath: basePath +  '/admin/api',
    }),
    new AdminUIApp({
      adminPath: basePath +  '/admin',
      apiPath: basePath  +  '/admin/api',
      graphiqlPath: basePath +  '/admin/api',
      enableDefaultRoute: true,
      authStrategy: NO_AUTH_MODE ? undefined : authStrategy,
    }),
  ],
  configureExpress: app => {
    app.set('trust proxy', true);
    app.set('secure', false);
  },
};
