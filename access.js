
// Access control functions
const userIsAdmin = ({ authentication: { item: user } }) => Boolean(user && user.isAdmin);
const userIsRegistered = ({ authentication: { item: user } }) => Boolean(user && user.email);
const userOwnsItem = ({ authentication: { item: user } }) => {
  if (!user) {
    return false;
  }

  // Instead of a boolean, you can return a GraphQL query:
  // https://www.keystonejs.com/api/access-control#graphqlwhere
  return { author: {id: user.id}};
};
const ownsParentItem = ({ authentication: { item: user } }) => {
  if (!user) {
    return false;
  }

  return { treeAuthor: {id: user.id}};
};
const userIsItem = ({ authentication: { item: user } }) => {
    if (!user) {
      return false;
    }
  
    // Instead of a boolean, you can return a GraphQL query:
    // https://www.keystonejs.com/api/access-control#graphqlwhere
    return { id: user.id };
  };
  
const userIsAdminOrOwner = auth => {
  const isAdmin = access.userIsAdmin(auth);
  const isOwner = access.userOwnsItem(auth);
  return isAdmin ? isAdmin : isOwner;
};

const userIsAdminOrInReadAccess = auth => {
  const isAdmin = access.userIsAdmin(auth);
  const hasReadAccess = access.userInReadAccess(auth);
  return isAdmin ? true : hasReadAccess;
};

const userIsAdminOrInWriteAccess = auth => {
  const isAdmin = access.userIsAdmin(auth);
  const hasWriteAccess = access.userInWriteAccess(auth);
  return isAdmin ? true : hasWriteAccess;
};
const userIsAdminOrOwnsParentItem = auth => {
  const isAdmin = access.userIsAdmin(auth);
  const isParentItemAuthor = access.ownsParentItem(auth);
  return isAdmin ? true : isParentItemAuthor;
}
const userInReadAccess = ({ authentication: { item: user } }) => {
  if (!user) {
    return false;
  }
  return {readAccess_some: {id: user.id}};
};

const userInWriteAccess = ({ authentication: { item: user } }) => {
  if (!user) {
    return false;
  }
  return { writeAccess_some: {id: user.id}};
};

const userIsAdminOrSelf = auth => {
    const isAdmin = access.userIsAdmin(auth);
    const isSelf = access.userIsItem(auth);
    return isAdmin ? isAdmin : isSelf;
  };
const access = { userIsAdmin, userOwnsItem, userIsAdminOrInReadAccess, userIsAdminOrOwnsParentItem, ownsParentItem, userInReadAccess, userIsAdminOrInWriteAccess, userInWriteAccess, userIsAdminOrOwner, userIsAdminOrSelf, userIsItem, userIsRegistered};
module.exports = access;