const { Text, Integer, Relationship, Select } = require('@keystonejs/fields');
 
module.exports = {
   fields: {
       name: {
           type: Text,
           isRequired: true,
       },
       nodeType: {
            type: Relationship, ref: 'nodeType', many:true,
            isRequired: true,
       }
    },
};