const { Text, Integer, Relationship } = require('@keystonejs/fields');
const access = require('../access.js');
const NO_AUTH_MODE = process.env.NO_AUTH_MODE == 'true' || process.env.NO_AUTH_MODE == 1;

module.exports = {
    fields: {
        name: {
            type: Text,
            isRequired: false,
        },
        description: {
            type: Text,
            isRequired: false,
        },
        mapComposition: {
            type: Text,
            isRequired: false,
        },
        nameLocal: {
            type: Text,
            isRequired: false,
        },
        descriptionLocal: {
            type: Text,
            isRequired: false,
        },
        author: {
            type: Relationship, ref: 'User', many: false,
            isRequired: false,
        },
        nodeTypeSet: {
            type: Relationship, ref: 'nodeTypeSet', many: false,
            isRequired: true,
        },
        alternativeLanguage: {
            type: Text,
            isRequired: false,
        },
        readAccess: {
            type: Relationship, ref: 'User', many:true,
            isRequired: true,
        },
        writeAccess: {
            type: Relationship, ref: 'User', many:true,
            isRequired: true,
        }
    },
    access: NO_AUTH_MODE ? undefined : {
        create: access.userIsRegistered,
        read: access.userIsAdminOrInReadAccess,
        update: access.userIsAdminOrInWriteAccess,
        delete: access.userIsAdminOrOwner,
    }
};
