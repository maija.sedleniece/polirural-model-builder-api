const { Text, Relationship, DateTime, Url, Float} = require('@keystonejs/fields');
 
module.exports = {
   fields: {
       language: {
           type: Text,
           isRequired: false,
       },
       locCoords: {
        type: Text,
        isRequired: false,
    },
        polarity: {
            type: Float,
            isRequired: false,
        },
        resourceURI: {
            type: Text,
            isRequired: false,
        },
        url: {
            type: Url,
            isRequired: false,
        },
        text: {
            type: Text,
            isRequired: false,
        },
        topics: {
            type: Text,
            isRequired: false,
        },
        sourceType: {
            type: Text,
            isRequired: false,
        },
        createdAt: {
            type: DateTime,
            isRequired: false,
        },
        updatedAt: {
            type: DateTime,
            isRequired: false,
        },
       node: {
           type: Relationship, ref: 'Node', many:false,
           isRequired: false,
       },
       tree: {
        type: Relationship, ref: 'Tree', many:false,
        isRequired: true,
        },
   },
};