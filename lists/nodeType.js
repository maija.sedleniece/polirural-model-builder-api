const { Text, Integer, Relationship, Select } = require('@keystonejs/fields');
const { Color } = require('@keystonejs/fields-color');

module.exports = {
   fields: {
       name: {
           type: Text,
           isRequired: true,
       },
       description: {
        type: Text,
        isRequired: false,
        },
       color: {
            type: Color,
            isRequired: true,
       }
    },
};