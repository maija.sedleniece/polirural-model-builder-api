const { Text, Relationship } = require('@keystonejs/fields');
 
module.exports = {
    fields: {  
    fromNode: {
           type: Relationship, ref: 'Node', many:false,
           isRequired: true,
       },
       toNode: {
        type: Relationship, ref: 'Node', many:false,
        isRequired: true,
        },
        tree: {
            type: Relationship, ref: 'Tree', many:false,
            isRequired: true,
        },
        value: {
            type: Text,
            isRequired: false,
        },
        label: {
            type: Text,
            isRequired: false,
        },
        description: {
            type: Text,
            isRequired: false,
        },
        valueLocal: {
            type: Text,
            isRequired: false,
        },
        labelLocal: {
            type: Text,
            isRequired: false,
        },
        descriptionLocal: {
            type: Text,
            isRequired: false,
        },
    },
};