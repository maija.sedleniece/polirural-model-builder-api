const { Relationship } = require('@keystonejs/fields');
const access = require('../access.js');
const NO_AUTH_MODE = process.env.NO_AUTH_MODE == 'true' || process.env.NO_AUTH_MODE == 1;

module.exports = {
    fields: {
        user: {
            type: Relationship, ref: 'User', many: false,
            isRequired: true,
        },
        tree: {
            type: Relationship, ref: 'Tree', many: false,
            isRequired: false,
        },
        treeAuthor: {
            type: Relationship, ref: 'User', many: false,
            isRequired: true,
        }
    },
    hooks: {
        afterChange: async ({
            originalInput,
            context,
        }) => {
            await updateReadWriteAccess(context, originalInput.tree.connect.id);
        },
        afterDelete: async ({
            existingItem,
            context,
        }) => {
            await updateReadWriteAccess(context, existingItem.tree.toString());
        }
    },
    access: NO_AUTH_MODE ? undefined : {
        create: access.userIsRegistered,
        read: access.userIsRegistered,
        update: access.userIsAdminOrOwnsParentItem,
        delete: access.userIsAdminOrOwnsParentItem,
    }
}

async function updateReadWriteAccess(context, treeId) {
    try {
        const response = await context.executeGraphQL({
            query: `query getList($where: TreeUserWhereInput) {      
                      allTreeUsers(where: $where) {
                          id
                          user {id}
                          tree {id}
                        }          
                      }`,
            variables: {
                where: {
                    "tree": {
                        "id": treeId
                    }
                }
            }
        });
        const query = {
            "operationName": "update",
            query: `mutation update($id: ID!, $data: TreeUpdateInput) {
                        updateTree(id: $id, data: $data) {
                              id
                              __typename
                              }
                            }
                            `,
            variables: {
                "id": treeId, "data": {
                    "readAccess": {
                        "disconnectAll": true,
                        "connect": response.data.allTreeUsers.map(treeUser => {
                            return { "id": treeUser.user.id };
                        })
                    },
                    "writeAccess": {
                        "disconnectAll": true,
                        "connect": response.data.allTreeUsers.map(treeUser => {
                            return { "id": treeUser.user.id };
                        })
                    }
                }
            },
        };
        await context.executeGraphQL(query);
    } catch (ex) {
        console.error(ex);
    }
}
