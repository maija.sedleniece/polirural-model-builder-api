const { Text, Checkbox, Relationship, DateTime, Url} = require('@keystonejs/fields');

 
module.exports = {
   fields: {
       contentType: {
           type: Text,
           isRequired: false,
       },
       createdAt: {
            type: DateTime,
            isRequired: false,
        },
        updatedAt: {
            type: DateTime,
            isRequired: false,
        },
        language: {
            type: Text,
            isRequired: false,
        },
        isActive: {
            type: Checkbox,
            isRequired: false,
        },
        resourceURI: {
            type: Text,
            isRequired: false,
        },
        url: {
            type: Url,
            isRequired: false,
        },
        sourceType: {
            type: Text,
            isRequired: false,
        },
       node: {
           type: Relationship, ref: 'Node', many:false,
           isRequired: false,
       },
   },
};