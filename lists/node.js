const { Text, Integer, Relationship, Url } = require('@keystonejs/fields');

module.exports = {
    fields: {
        name: {
            type: Text,
            isRequired: false,
        },
        nameLocal: {
            type: Text,
            isRequired: false,
        },
        nodeType: {
            type: Relationship, ref: 'nodeType', many: false,
            isRequired: true,
        },
        keywords: {
            type: Text,
            isRequired: false,
        },
        description: {
            type: Text,
            isRequired: false,
        },
        keywordsLocal: {
            type: Text,
            isRequired: false,
        },
        descriptionLocal: {
            type: Text,
            isRequired: false,
        },
        url: {
            type: Url,
            isRequired: false,
        },
        tree: {
            type: Relationship, ref: 'Tree', many: false,
            isRequired: true,
        },
    },
};